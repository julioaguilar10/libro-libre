# Libro Libre

**Libro Libre** es un proyecto en desarrollo utilizando Django. La aplicación tiene como objetivo gestionar información sobre libros, usuarios y registros de préstamos y devoluciones de manera eficiente.

## Funcionalidades

La aplicación manejará la siguiente información:

- **Detalles de libros**: título, autor, género, entre otros.
- **Información de usuarios**: nombre y correo electrónico.
- **Registros de préstamos y devoluciones**.

## Licencia

Este proyecto está bajo  licencia GPL, lo que permite su uso, modificación y distribución de forma abierta.

